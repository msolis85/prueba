const configHttpRequest = (function () {
    'use strict';

    /**
     * Crear una instancia del objeto XMLHttpRequest y poder realizar las peticiones de http
     */
    function getConfig() {
        let  http_request = null;

        if (window.XMLHttpRequest) { //Mozilla, Safari, ...
            http_request = new XMLHttpRequest();
            if (http_request.overrrideMimeType) {
                http_request.overrrideMimeType('text/xml');
            };
        } else if(window.ActiveXObject) { //IE
            try {
                http_request = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e) {
                try {
                    http_request = new ActiveXObject("Microsoft.XMLHTTP")
                }catch (e) {}
            };
        };

        if(!http_request) {
            return null;
        };

        return http_request;
    }

    return {
        getConfig
    }
})();