const getCall = (function () {
    'use strict';
    
    /**
     * Obtiene los post realizados en el sitio de WP del cliente
     * @param { string} params 
     */
    function get(url = '', responseHandle) { //Se toman los parámetros de los default de la api de WP
        /* let url = `${url}?page=${params.number}&per_page=${params.perPage}&order=${params.order}`; */
        let http_request = configHttpRequest.getConfig();

        if(http_request) {
            http_request.onreadystatechange = function () {  //Se asigna la función para manejar la respuesta que se obtenga
                if(http_request.readyState == 4 ) {
                    responseHandle(JSON.parse(http_request.responseText));
                };
            }; 
            http_request.open('GET', url, true);
            http_request.send();
        }else {
            return null;
        };
    }

    return {
        get
    }

})();