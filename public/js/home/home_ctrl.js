const homeCtrl = (function () {
    'use strict';

    /**
     * Se obtienen los post que se van a mostrar en el home del sitio
     */
    function getPostToShow () {
       const response = getCall.get('https://chimeraofdesign.com/wp-json/wp/v2/posts?order=desc&page=4&per_page=1', responseHandle);
    }

    /**
     * Se obtienen los productos para el front
     */
    function getProducts () {
       // const response = getCall.get('https://dl.dropboxusercontent.com/s/jl5iqdxvuclj0jc/bestSellingProducts.json?dl=0', responseHandle);
       const response = getCall.get('https://dl.dropboxusercontent.com/s/orehjx0q2x2zfqp/bestSellingProducts.json?dl=0', responseHandle);
    }

    /**
     * Se maneja la respuesta de una llamada por http
     * @param {{}} response 
     */
    function responseHandle(response) {
        //Aquí se debe manejar la respuesta
        //console.log(response);
        var objetoJson = response;
        var prodIndividual = objetoJson['products'];
        var resena = objetoJson['reseñas'];
        var a; 
        var b;  
        var medicion = '';      
        for(a = 0; a <= prodIndividual.length; a++){
           var porcentaje = Math.round(prodIndividual[a].specialPrice * 100 / prodIndividual[a].price);
           for(b = 0; b <= resena.length; b++){
              try {
                if(prodIndividual[a].id == resena[b].id){                    
                    switch(resena[b].stars){
                        default: 
                            medicion = "<div class='row'><div class='col-sm-7 addToCart' id='addToCart"+resena[b].id+"'>Add To Cart</div><div class='col-sm-5'><div class='ec-stars-wrapper' title='Por definir'><a href='#' data-value='1'>&#9733;</a><a href='#' data-value='2'>&#9733;</a><a href='#' data-value='3'>&#9733;</a><a href='#' data-value='4'>&#9733;</a><a href='#' data-value='5'>&#9733;</a></div></div>";
                        break;
                        case 1: 
                            medicion = "<div class='row'><div class='col-sm-7 addToCart' id='addToCart"+resena[b].id+"'>Add To Cart</div><div class='col-sm-5'><div class='ec-stars-wrapper' title='Calificación 1 estrella'><a href='#' data-value='1' class='ec-stars-active'>&#9733;</a><a href='#' data-value='2'>&#9733;</a><a href='#' data-value='3'>&#9733;</a><a href='#' data-value='4'>&#9733;</a><a href='#' data-value='5'>&#9733;</a></div></div>";
                        break;
                        case 2: 
                            medicion = "<div class='row'><div class='col-sm-7 addToCart' id='addToCart"+resena[b].id+"'>Add To Cart</div><div class='col-sm-5'><div class='ec-stars-wrapper' title='Calificación 2 estrellas'><a href='#' data-value='1' class='ec-stars-active'>&#9733;</a><a href='#' data-value='2' class='ec-stars-active'>&#9733;</a><a href='#' data-value='3'>&#9733;</a><a href='#' data-value='4'>&#9733;</a><a href='#' data-value='5'>&#9733;</a></div></div>";
                        break;
                        case 3: 
                            medicion = "<div class='row'><div class='col-sm-7 addToCart' id='addToCart"+resena[b].id+"'>Add To Cart</div><div class='col-sm-5'><div class='ec-stars-wrapper' title='Calificación 3 estrellas'><a href='#' data-value='1' class='ec-stars-active'>&#9733;</a><a href='#' data-value='2' class='ec-stars-active'>&#9733;</a><a href='#' data-value='3' class='ec-stars-active'>&#9733;</a><a href='#' data-value='4'>&#9733;</a><a href='#' data-value='5'>&#9733;</a></div></div>";
                        break;
                        case 4: 
                            medicion = "<div class='row'><div class='col-sm-7 addToCart' id='addToCart"+resena[b].id+"'>Add To Cart</div><div class='col-sm-5'><div class='ec-stars-wrapper' title='Calificación 4 estrellas'><a href='#' data-value='1' class='ec-stars-active'>&#9733;</a><a href='#' data-value='2' class='ec-stars-active'>&#9733;</a><a href='#' data-value='3' class='ec-stars-active'>&#9733;</a><a href='#' data-value='4' class='ec-stars-active'>&#9733;</a><a href='#' data-value='5'>&#9733;</a></div></div>";
                        break;
                        case 5:
                            medicion = "<div class='row'><div class='col-sm-7 addToCart' id='addToCart"+resena[b].id+"'>Add To Cart</div><div class='col-sm-5'><div class='ec-stars-wrapper' title='Calificación 5 estrellas'><a href='#' data-value='1' class='ec-stars-active'>&#9733;</a><a href='#' data-value='2' class='ec-stars-active'>&#9733;</a><a href='#' data-value='3' class='ec-stars-active'>&#9733;</a><a href='#' data-value='4' class='ec-stars-active'>&#9733;</a><a href='#' data-value='5' class='ec-stars-active'>&#9733;</a></div></div>"; 
                        break;
                    }
                }
              } catch (e) {    
                //console.log(b);
              }
                
           }
           $("#ws").append(						
                "<div class='col-sm-3' onmouseover='hover("+prodIndividual[a].id+")' onmouseout='out("+prodIndividual[a].id+")'><span class='webService'><div class='galeria2' style='background-image:url(imgs/"+prodIndividual[a].id+".jpg);'><div class='porcentaje'>"+porcentaje+"%</div> </div><div class='container-fluid'><div class='row'><div class='col-sm-8' style='font-size:14px; font-weight:bolder;'>"+prodIndividual[a].name+"</div><div class='col-sm-4' style='font-weight:bolder;'>"+prodIndividual[a].specialPrice+"</div></div><div class='row'><div class='col-sm-8'>"+prodIndividual[a].name+"</div><div class='col-sm-4 tachado'>"+prodIndividual[a].price+"</div></div></div><div class='caption' style='padding:0px 15px;'>"+medicion+"</div></div></div></span></div>"
            );        
        }        
    }

    return {
        getPostToShow,
        getProducts
    }

})();