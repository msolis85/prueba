const gulp = require('gulp'),
sass = require('gulp-sass'),
refresh = require('gulp-refresh');

gulp.task('default',  () => {
  gulp
    .src('*.html')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('dist'))
    .pipe(refresh())
    console.log('todo ok');
})
 
gulp.task('watch', () => {
  refresh.listen()
  gulp.watch('*.html', ['default'])
})