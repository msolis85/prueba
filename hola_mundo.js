/*var http = require('http'),
      fs = require('fs'),
      path = require('path');
fs.readFile('index.html', function(error, html){
  http.createServer(function(req, res){
    res.write(html);
    res.end();
  }).listen(8080);
});*/
// Se cargan los modulos necesarios.
const http = require('http');
const express = require('express');
const path = require('path');

// Crea una Express app.
var app = express();

// obtiene la ruta del directorio publico donde se encuentran los elementos estaticos (css, js).
var publicPath = path.resolve(__dirname, 'public'); //path.join(__dirname, 'public'); también puede ser una opción

// Para que los archivos estaticos queden disponibles.
app.use(express.static(publicPath));

app.get('/', function(req, res){
  res.sendfile(__dirname + '/index.html');
  });

app.listen(8080);